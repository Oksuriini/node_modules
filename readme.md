## Very cool test module

1. Download this stuff: ``git clone https://gitlab.com/Oksuriini/node_modules``

2. Use command ``npm run start`` to start server

3. Use command ``curl localhost:3000`` or connect with browser to https://localhost:3000

4. Other functions can be accessed by using the ```localhost:3000/add?a=10&b=10```
```localhost:3000/subtract?z=10&x=10```
and 
```localhost:3000/localtime```
and
```localhost:3000/pizzaslice?n=100```

5. You can change the values of a,b,z and x as you please.
