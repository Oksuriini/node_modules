const expect = require('chai').expect;
const assert = require('chai').assert;
const afterEach = require('mocha').afterEach;
const should = require('chai').should();
const mylib = require('../src/mylib')


describe('Unit testing mylib.js',() => {
    let testcount = 0;
    let myvar = undefined;
    before(() => {
        myvar = 1;
        timebefore = mylib.localtime();
        console.log(timebefore);
    })
    it("Should return 3 when using sum function with a=1, b=2", () =>{
        const result = mylib.sum(1,2)
        expect(result).to.equal(3)
    })
    it("Should return 5 when using subtract function with z=10, x=5", () =>{
        const subtraction = mylib.subtract(10,5);
        expect(subtraction).to.equal(5);
    })
    it("Myvar should exist", () => {
        should.exist(myvar)
    })
    it("Assert foo is not bar", () => {
        assert("foo" !== "bar")
    })
    it("This test should fail",() =>{
        should.exist(notexist)
    })
    it("Shoud return around 96.2 when using pizzaslice function with n=10",()=>{
        const pizzatime = mylib.pizzaslice(10);
        console.log("pizza size: " + pizzatime)
        expect(pizzatime).to.be.closeTo(96.1, 96.3);
    })
    afterEach(() =>{
        testcount = testcount +1;
    })
    after(() => {
        let notexist = 10;
        timeafter = mylib.localtime();
        console.log(timeafter);
        console.log("Test count:" + testcount)
    })
})