const express = require('express')
const mylib = require('./mylib')
const app = express()
const port = 3000



// endpoint localhost:3000/
app.get('/', (req, res) =>{
    res.send('Hello')
})

//endpoint localhost:3000/add?a=42&b=21
app.get('/add', (req, res) => {
    const a = parseInt(req.query.a); 
    const b = parseInt(req.query.b);
    console.log({a, b})
    const total = mylib.sum(a,b)
    res.send("add works " + total.toString())
})

//endpoint localhost:3000/subtract?z=22&x=12
app.get('/subtract', (req, res)=>{
    const z = parseInt(req.query.z);
    const x = parseInt(req.query.x);
    console.log({z, x});
    const subtracted = mylib.subtract(z,x);
    res.send("subtraction done: " + subtracted.toString());
})

//endpoint localhost:3000/pizzaslice?n=100
app.get('/pizzaslice',(req,res)=>{
    const n = parseInt(req.query.n);
    const area = mylib.pizzaslice(n);
    res.send("One pizza slice has the are of: " + area + "cm^2")
})

//endpoint localhost:3000/localtime
app.get('/localtime', (req, res)=>{
    console.log()
    const timerightnow = mylib.localtime();
    res.send("Current time is: " + timerightnow.toString())
})


app.listen(port, () => {
    console.log(`Example app listening on port ` + port)
})