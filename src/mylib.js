module.exports = {
    //Sum b to a
    sum: (a,b) => {
           return a +b;
    },
    //Subtract x from z
    subtract: (z,x) =>{
        return (z-x);
    },
    //Tells localtime and date
    localtime: () =>{
        let time = new Date()
        return time;
    },
    //Tells the size of one pizzaslice of 17.5 cm pizza, when cut in n amount of equal sized slices
    pizzaslice: (n) =>{
        let r = 17.5;
        let pi = Math.PI;
        let area = ((360 / n)/360) * pi * (r*r);
        return area
    }

}